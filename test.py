''' 
    Instruction of use:

    1. create two sets of objects; 
       click SPACE to add a sample of photo;
       click ESC to end collection of one set and start another;
       your data is stored inside directory ./data;

    2. extract features by the part of # classification object;

    3. test the model by the part in the end;
       end the program by clicking button Q;

    4. you could create more then two sets in the beginning

    The defined functions are inside API.py;
    For functions in more detail, they are inside directory .utils.
'''

from API import *

# Demo on how API here works:
if __name__ == '__main__':

    labels = []
    imgs = []
    # first set of image
    for i in range(1):
        myname, mynum = capImage()
        labels.extend(add_label(myname, mynum))
        imgme = load_image_folder(myname)
        imgs += imgme
    net_handle = Classification()

    
    hog, features = [], []
    
    for i in imgs:
        img_hog = get_hog(i)
        img_feature = net_handle.get_feature(i)
        hog.append(img_hog)
        features.append(img_feature)

    handle = LinearClassifier()
    handle.train(features, labels)

    ### test if the trained model is well enough
    test_live(handle)

    ### clear data 
    #shutil.rmtree(os.path.join(DATA_PATH,myname))
    #shutil.rmtree(os.path.join(DATA_PATH,cupname))
    '''
    # second set of image
    cupname, cupnum = capImage()
    labels.extend(add_label(cupname, cupnum))
    imgcup = load_image_folder(cupname) 
    '''
    # integral of two sets
    #imgs = imgme + imgcup

    # classification object
    