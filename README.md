# classification on SenseStorm

## Preparation

1. clone the repo
```
git clone https://gitlab.com/sensestudyhk/sensestorm/classification.git
```

2. We need to install some missing packages before using the code

```
sudo apt-get remove numpy

pip3 install numpy

pip3 install --upgrade numpy

pip3 install scikit-image

```

## Usage of the repo
Please see what's inside test.py

Instruction of use:

    1. create two sets of objects; 
       click SPACE to add a sample of photo;
       click ESC to end collection of one set and start another;
       your data is stored inside directory ./data;

    2. extract features by the part of # classification object;

    3. test the model by the part in the end;
       end the program by clicking button Q;

    4. you could create more then two sets in the beginning