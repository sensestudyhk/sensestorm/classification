import os
import shutil
import pickle
import cv2
import numpy as np
import time
from sensestorm import getDistance
from CourseHeader.API import show_image
from skimage import feature as ft
from PIL import Image
from CourseHeader.API import show_image
from utils.filename_handle import get_paths
from utils.data_handle import dataHandle, capImage
from utils.mobileNet import MobileNetDetection
from utils.hash_acceleration import get_img_hash, get_dir
from utils.linear_classify import LinearClassifier
from utils.file_transfer import *

BASE_PATH, MODEL_PATH, DATA_PATH = get_paths()

class Classification(MobileNetDetection):
    def __init__(self, thresh=0.7):
        super(Classification, self).__init__()

    def get_feature(self, img):
        # print("please wait...")
        self.detected_list = []
        self.cvNet.setInput(cv2.dnn.blobFromImage(img, size=(300, 300), swapRB=True, crop=False))
        cvOut = self.cvNet.forward('FeatureExtractor/MobilenetV1/MaxPool2d_4_2x2/MaxPool')
        return cvOut.reshape(-1)

# inheritated from utils.data_handle.datahandle
class handleImage(dataHandle):
    def __init__(self):
        dataHandle.__init__(self)



def load_image_folder(path):
    images = []
    path = os.path.join(DATA_PATH, path)
    for filename in os.listdir(path):
        image = cv2.imread(os.path.join(path, filename))
        if image is not None:
            image = cv2.resize(image, (300, 300), fx=0.5,fy=0.5, interpolation = cv2.INTER_CUBIC)
            images.append(image)
    if images is None:
        print('The returned list is empty')
    return images

    
def show(img):
    image = Image.fromarray(img, 'RGB')
    image.show()


def get_hog(img):
    features, hog_img = ft.hog(
        img,  # input image
        orientations=9,  # number of bins
        pixels_per_cell=(10, 10),  # pixel per cell
        cells_per_block=(1, 1),  # cells per blcok
        block_norm = 'L1',  # block norm : str {‘L1’, ‘L1-sqrt’, ‘L2’, ‘L2-Hys’}
        # power law compression (also known as gamma correction)
        transform_sqrt=True,
        feature_vector=True,  # flatten the final vectors
        visualize=True)  # return HOG map
    return (features, hog_img)


def show_hog(hog):
    features, hog_img = hog
    hog_img.show()

def add_label(name, folder):
    DIR = '/home/pi/Desktop/classification/data/{0}'.format(folder)
    num = len([f for f in os.listdir(DIR) if os.path.isfile(os.path.join(DIR, f))])
    return [name] * int(num)
    
def test_images(handle, test_imgs):
    net_handle = Classification()
    for item in test_imgs:
        show_image(item)
        img_feature = net_handle.get_feature(item).reshape(1, -1)
        res = handle.pred(img_feature)
        print(res[0])
        time.sleep(3)
        

    
def test_live(handle):
    cam = cv2.VideoCapture(0)
    cv2.namedWindow("Captured Objects")
    net_handle = Classification()
    while True:
        ret, frame = cam.read()
        dist = getDistance()
        if dist > 8 and dist < 10:
            feature = net_handle.get_feature(frame).reshape(1, -1)
            res = handle.pred(feature)
            labeltext = res[0]
            print(labeltext)
            cv2.putText(frame, labeltext, (5, 10), cv2.FONT_HERSHEY_COMPLEX, 1.0, (0,255,0), 1)
        show_image(frame)
        #cv2.imshow("Captured Objects", frame)
        time.sleep(1)

        if cv2.waitKey(1) == ord('q'):
            break

    cam.release()
    cv2.destroyAllWindows()



