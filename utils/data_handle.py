import cv2
import os
import shutil
from .filename_handle import get_paths

class dataHandle(object):
    def __init__(self) -> None:
        super().__init__()
        
    def inputObject(self):
        info = ["Press SPACE to take a photo \n",
                "Press ESC to stop taking one object \n",
                "You need to click the image Window to do the above operation \n"]
        print(*info)
        
        self.name = input("Please input the object name: ")
        if self.name in ['cat', 'dog', 'panda', 'rabbit', 'test', 'hashed_feature']:
            self.name = input('Please re-enter the name since this one has been taken.')

        # change working directory to data
        doncare1, doncare2, original_data_path = get_paths()
        self.data_path = original_data_path + '/{}'.format(self.name)

        if not os.path.exists(self.data_path):
            os.mkdir(self.data_path)
        os.chdir(self.data_path)


    def writeObject(self):
        # image counting for naming purpose
        img_counter = 0
        print('Start collecting image of ', self.name)
        cam = cv2.VideoCapture(0)
        cv2.namedWindow("Captured Objects")
        while True:
            ret, frame = cam.read()
            if not ret:
                print("failed to grab frame")
                break
            cv2.imshow("Captured Objects", frame)

            # use Keys to trigger different events
            k = cv2.waitKey(1)
            if k == 27:
                # ESC pressed
                print("Escape hit, closing...")
                break
            elif k == 32:
                # SPACE pressed
                img_counter += 1
                img_name = self.name + "{}.png".format(img_counter)
                cv2.imwrite(img_name, frame)
                print("{} written!".format(img_name))
                
        self.number = img_counter
        cam.release()
        cv2.destroyAllWindows()


    def removeObject(self, name):
        input_name = name
        data_path = '../data/{}/'.format(input_name)
        print(os.getcwd())
        if os.path.isdir(data_path):
            shutil.rmtree(data_path)
            print('The dataset {} has been removed'.format(input_name))
        else:
            print('The data set does not exist or cannot be removed')


def capImage():
    # current object name
    thisobject = dataHandle()
    thisobject.inputObject()
    thisobject.writeObject()
    print('There are {} '.format(thisobject.number) + thisobject.name + ' captured')
    info = [thisobject.name, thisobject.number]
    del thisobject
    return info

# test the functionality of data_handle functionss   
if __name__ == '__main__':
    name, path, num = capImage()
    print('done')
