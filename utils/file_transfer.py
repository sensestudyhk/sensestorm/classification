def filemove(file_name):
	import os
	import shutil
	import warnings
     
	usb_path = os.popen('ls -d /media/pi/*').read()
	if usb_path:
	  # directory on USB drive
		dir_usb_path = usb_path.replace('\n', '/'+file_name)
     
	  # target location on SenseStorm
		dir_pi_path = '/home/pi/Desktop/classification/data/{0}'.format(file_name)
		if os.path.isdir(dir_pi_path):
			shutil.rmtree(dir_pi_path)
		shutil.copytree(dir_usb_path,dir_pi_path)
		print('Folder {0} has been moved to SenseStorm.'.format(file_name))
	else:
		print('Please plug-in your USB drive and try agian')
		print(usb_path)

def file_remove(file_name):
	import os
	import shutil
	
	dir_pi_path = '/home/pi/Desktop/classification/data/{0}'.format(file_name)
	if os.path.isdir(dir_pi_path):
		shutil.rmtree(dir_pi_path)
		print('Folder {0} has been removed.'.format(file_name))
	else:
		print('Folder {0} does not exist.'.format(file_name))

def remove_all():
	import os
	import shutil
	
	data_path = '/home/pi/Desktop/classification/data'
	shutil.rmtree(data_path)
	os.mkdir(data_path)
	print('All folders inside sensestorm are cleared.')
	
if __name__ == '__main__':
	filemove('test')
