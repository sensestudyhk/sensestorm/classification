from API import *

def extract_feature(imgs):
    net_handle = Classification()
    return net_handle.get_feature(imgs)

def train(model, feature, label):
    model.train(feature, label)
    print('Your Classification Model is ready to use !')

